﻿using UnityEngine;

public class MainModel : MainElement
{
    [Header("You can change all variables only from script")]
    [SerializeField]
    string empty;

    [HideInInspector]
    public float camSens = 0.20f; // How sensitive it with mouse
    [HideInInspector]
    public float boatSpeed = 400f; // Boat speed
    [HideInInspector]
    public float minSpawnTime = 2f; // Min boat spawn time
    [HideInInspector]
    public float maxSpawnTime = 5f; // Max boat spawn time
    [HideInInspector]
    public bool gamePlaying = false;
    [HideInInspector]
    public enum BoatType { small, medium, large };
    [HideInInspector]
    public int[] boatHealth = { 1, 2, 3 };
    [HideInInspector]
    public float ballForce = 10f;

    public bool findFloor;
}
