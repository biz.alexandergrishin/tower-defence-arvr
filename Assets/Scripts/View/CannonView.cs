﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonView : MainElement
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (!app.model.findFloor)
        {
            yield return null;
        }
        app.Notify(MainNotifications.game_start, this);
    }
}
