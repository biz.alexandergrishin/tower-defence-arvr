﻿using System;
using UnityEngine;
using static MainModel;

public class BoatView : MainElement
{
    [SerializeField]
    public BoatType boatType;

    // Start is called before the first frame update
    void Start()
    {
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "cannonBall")
        {
            Debug.Log("Collider entered. Boat type: " + Enum.GetName(typeof(BoatType), boatType));
            app.Notify(MainNotifications.boatShot, gameObject, Enum.GetName(typeof(BoatType), boatType));
        } else if (other.gameObject.tag == "safeZone")
            app.Notify(MainNotifications.boatSafeZone, gameObject);

    }
}
