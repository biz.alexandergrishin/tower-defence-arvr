﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainView : MainElement
{
    public Camera c_camera;
    public Camera ar_camera;

    public CannonView cannon;

    public GameObject smallBoat;
    public GameObject mediumBoat;
    public GameObject largeBoat;

    public GameObject ballObject;
    public GameObject ballTarget;

    public GameObject floor;
}
