﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingTile : MonoBehaviour
{
    private Settings settings;

    private float duration;

    [HideInInspector]
    public MeshRenderer[] meshRenderers;

    private void Start()
    {
        this.meshRenderers = GetComponentsInChildren<MeshRenderer>();
    }

    public void Init(Settings settings)
    {
        this.settings = settings;
        UpdatePosition();
    }

    private void Update()
    {
        duration += Time.deltaTime;
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        if (settings != null)
        {
            float y = Mathf.Lerp(settings.startHeight, 0, settings.curve.Evaluate(duration));
            transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
        }
    }

    [System.Serializable]
    public class Settings
    {
        public float startHeight;
        public AnimationCurve curve;
    }
}
