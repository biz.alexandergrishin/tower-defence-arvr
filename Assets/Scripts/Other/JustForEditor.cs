﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustForEditor : MonoBehaviour
{


	public GameObject Camera1;
	public GameObject Camera2;
	void Start ()
	{
        //Crashlytics test 
        //Crashlytics.Crash();
        //Crashlytics.ThrowNonFatal();
        #if UNITY_EDITOR
        Camera1?.SetActive(false);
		//tutorial?.SetActive(false);
		Camera2?.SetActive(true);
		return;
        #endif
        #if !UNITY_EDITOR
        Camera2?.SetActive(false);
		gameObject.SetActive(false);
        #endif
    }


}
