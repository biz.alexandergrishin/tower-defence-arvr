﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTestControll : MonoBehaviour
{
	private float inputX;
	private float inputZ;

	public float speed = 20;

	void Update () {
		inputX = Input.GetAxis ("Horizontal");
		inputZ = Input.GetAxis ("Vertical");
         
		if (inputX != 0)
			rotateX ();
		if (inputZ != 0)
			rotateZ ();
	}
 
	void rotateX()
	{
		transform.Rotate (new Vector3 (0f , inputX * Time.deltaTime * speed,0f));
	}
	
	void rotateZ()
	{
		transform.Rotate (new Vector3 ( inputZ * Time.deltaTime * speed, 0f,0f));
	}

}
