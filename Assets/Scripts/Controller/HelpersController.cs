﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpersController : MonoBehaviour
{
    public GameObject[] getAllChilds(Transform g_transform)
    {
        List<GameObject> childList = new List<GameObject>();

        foreach (Transform child in g_transform)
        {
            childList.Add(child.gameObject);
        }

        return childList.ToArray();
    }
}
