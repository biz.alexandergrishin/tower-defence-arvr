﻿using UnityEngine;

/// <summary>
/// Cannon controller. Using only at Editor.
/// </summary>
public class CannonController : MainElement
{
    private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)

    void Update()
    {
        lastMouse = Input.mousePosition - lastMouse;
        lastMouse = new Vector3(-lastMouse.y * app.model.camSens, lastMouse.x * app.model.camSens, 0);
        lastMouse = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + lastMouse.y, 0);
        transform.eulerAngles = lastMouse;
        //X rotation
        //if (transform.eulerAngles.x >= -15f && transform.eulerAngles.x <= 0f)
            //lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y, 0);
        transform.eulerAngles = lastMouse;
        lastMouse = Input.mousePosition;
    }
}
