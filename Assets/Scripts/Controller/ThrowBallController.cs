﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBallController : MainElement
{
    private Vector3 startPosition;

    // Use this for initialization
    void Start()
    {
        /* Increase Gravity */
        Physics.gravity = new Vector3(0, -20, 0);
    }

    // Update is called once per frame
    void Update()
    {
        startPosition = new Vector3(app.view.ballObject.transform.position.x,
            app.view.ballObject.transform.position.y,
            app.view.ballObject.transform.position.z);

        if (Input.GetMouseButtonDown(0))
        {
            FastThrow();
        }
    }

    void FastThrow()
    {
        var ball = Instantiate(app.view.ballObject, startPosition, app.view.ballObject.transform.rotation);
        ball.SetActive(true);
        if(app.view.c_camera.gameObject.activeSelf)
            ball.GetComponent<Rigidbody>().AddForce(app.view.c_camera.transform.forward, ForceMode.Impulse);
        if (app.view.ar_camera.gameObject.activeSelf)
            ball.GetComponent<Rigidbody>().AddForce(app.view.ar_camera.transform.forward, ForceMode.Impulse);
    }

    IEnumerator BallLifeTime(GameObject ball)
    {
        yield return new WaitForSeconds(4f);
        Destroy(ball);
    }


}
