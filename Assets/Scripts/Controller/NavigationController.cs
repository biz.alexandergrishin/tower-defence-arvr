﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationController : MainElement
{
    public void StartVRGame()
    {
        SceneManager.LoadSceneAsync(2);
    }

    public void StartARGame()
    {
        SceneManager.LoadSceneAsync(1);
    }
}
