﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MainModel;

public class BoatController : MainElement
{
    Vector3 target;
    int boatHealth;

    // Start is called before the first frame update
    void Start()
    {
        //Setting up health of current boat
        switch (gameObject.GetComponent<BoatView>().boatType)
        {
            case BoatType.small:
                boatHealth = app.model.boatHealth[0];
                break;
            case BoatType.medium:
                boatHealth = app.model.boatHealth[1];
                break;
            case BoatType.large:
                boatHealth = app.model.boatHealth[2];
                break;
            default:
                boatHealth = app.model.boatHealth[0];
                break;

        }

        //Correctly position of boat
        var buffer_vector = transform.position;
        transform.LookAt(app.view.cannon.gameObject.transform);
        transform.position = buffer_vector;
        transform.eulerAngles = new Vector3(-90f, transform.eulerAngles.y, transform.eulerAngles.z);
        target = new Vector3(app.view.cannon.transform.position.x, transform.position.y, app.view.transform.position.y);
    }

    public void Hit()
    {
        boatHealth--;
        if (boatHealth <= 0)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        float step = app.model.boatSpeed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, target, step); // move boat
    }
}
