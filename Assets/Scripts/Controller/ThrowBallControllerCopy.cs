﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBallControllerCopy : MainElement
{
    private Vector3 startPosition;

    // Use this for initialization
    void Start()
    {
        /* Increase Gravity */
        Physics.gravity = new Vector3(0, -20, 0);
    }

    // Update is called once per frame
    void Update()
    {
        startPosition = new Vector3(app.view.ballObject.transform.position.x,
            app.view.ballObject.transform.position.y,
            app.view.ballObject.transform.position.z);

        if (Input.GetMouseButtonDown(0))
        {
            FastThrow();
        }
    }

    void FastThrow()
    {
        var direction = app.view.c_camera.transform.forward;
        var ball = Instantiate(app.view.ballObject, startPosition, app.view.ballObject.transform.rotation);
        Vector3 v = (direction + Vector3.up).normalized;
        ball.SetActive(true);
        ball.GetComponent<Rigidbody>().AddForce(v * app.model.ballForce, ForceMode.Impulse);
    }


}
