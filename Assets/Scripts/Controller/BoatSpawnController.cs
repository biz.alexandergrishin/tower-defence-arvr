﻿using System.Collections;
using UnityEngine;

public class BoatSpawnController : MainElement
{

    public IEnumerator StartSpawn()
    {
        int transformX;
        int transformZ;
        while (app.model.gamePlaying)
        {
            yield return new WaitForSeconds(Random.Range(app.model.minSpawnTime, app.model.maxSpawnTime));
            GameObject boat = null;

            int boatV = Random.Range(1, 3);

            //Random for - or + spawn vectors value
            transformX = Random.Range(0, 2) == 0 ? 1 : -1;
            transformZ = Random.Range(0, 2) == 0 ? 1 : -1;

            //Setting up new boat position
            var boatV2 = Random.insideUnitCircle * 50;
            var boatP = new Vector3
                (boatV2.x * transformX,
                app.view.smallBoat.transform.position.y,
                boatV2.y * transformZ);

            if (boatV == 1)
                boat = Instantiate(app.view.smallBoat, boatP, app.view.smallBoat.transform.rotation, null);
            if (boatV == 2)
                boat = Instantiate(app.view.mediumBoat, boatP, app.view.smallBoat.transform.rotation, null);
            if (boatV == 3)
                boat = Instantiate(app.view.largeBoat, boatP, app.view.smallBoat.transform.rotation, null);

            boat?.AddComponent<BoatController>();

            boat?.SetActive(true);
        }
    }
}
