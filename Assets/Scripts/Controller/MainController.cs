﻿using System;
using UnityEngine;
using static MainModel;

public class MainController : MainElement
{
    private HelpersController helpersController;
    public BoatSpawnController boatSpawnController;

    // Start is called before the first frame update
    void Start()
    {
        helpersController = FindObjectOfType<HelpersController>();
        boatSpawnController = FindObjectOfType<BoatSpawnController>();
    }

    // Handles events here
    public void OnNotification(string p_event_path, object p_target, params string[] p_data)
    {
        //Handle Main Notifications 
        if (p_event_path == MainNotifications.game_start)
        {
            app.model.gamePlaying = true;
            StartCoroutine(boatSpawnController.StartSpawn());
        }

        //Handle Boat Nofitications
        if(p_event_path == MainNotifications.boatShot)
        {
            var boat = p_target as GameObject;
            boat.GetComponent<BoatController>().Hit();
        }

        if (p_event_path == MainNotifications.boatSafeZone)
        {
            Destroy(p_target as GameObject);
        }
    }
}
