﻿using System;
using static MainModel;

class MainNotifications : MainElement
{
    /* Main Game Notifications */
    static public string game_start = "game.start";
    /* Boat Notifications */
    static public string boatShot = "boat.shot";
    static public string boatSafeZone = "boat.safezone";
    /* ... */
}